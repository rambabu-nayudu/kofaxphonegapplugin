//
// License.js
//
//  Copyright (c) 2012 - 2017 Kofax. Use of this code is with permission pursuant to Kofax license terms.

var ActionUtils = require('./ActionUtils');

//License constructor and corresponding methods
/**
 * @class
 * @alias License
 * @constructor
 */
var License = function() {


};

//Get the License supported ServerTypes.
/**
 * @private
 * Get the License supported ServerTypes
 *
 * @example
 *  var LicObj = kfxCordova.kfxUtilities.createLicense();
 *  var serverTypes = LicObj.getServerTypes();
 *  serverTypes should be like below {
 *       RTTI            :  "RTTI",
 *       TOTALAGILITY    :  "TotalAgility"
 *   };
 */
License.prototype.getServerTypes = function() {
    var serverTypes = {
        RTTI: "RTTI",
        TOTALAGILITY: "TotalAgility"
    };
    return serverTypes;
};


//Get the License supported LicenseType.
/**
 * Get the License supported LicenseTypes
 *
 * @example
 * var LicObj = kfxCordova.kfxUtilities.createLicense();
 * var licenseTypes = LicObj.getLicenseTypes();
 * licenseTypes should be like below {
 *           IMAGE_CAPTURE       :  "IMAGE_CAPTURE",
 *           IMAGE_PROCESSING    :  "IMAGE_PROCESSING",
 *           BARCODE_CAPTURE     :   "BARCODE_CAPTURE",
 *           LOGISTICS           :  "LOGISTICS",
 *           ID_EXTRACTION       :  "ID_EXTRACTION"
 *   };
 */
License.prototype.getLicenseTypes = function() {
    var licenseTypes = {
        IMAGE_CAPTURE: "IMAGE_CAPTURE",
        IMAGE_PROCESSING: "IMAGE_PROCESSING",
        BARCODE_CAPTURE: "BARCODE_CAPTURE",
        LOGISTICS: "LOGISTICS",
        ID_EXTRACTION: "ID_EXTRACTION"
    };
    return licenseTypes;
};

/// To set the License of the native SDK.
/**
 * Method to set the License of the native SDK.
 *
 * The input to this method is the VRS license string. If the license string is valid and days remain in the license, this method returns KMC_SUCCESS .
 * If the license string is valid but no days remain in the license, this method returns KMC_IP_LICENSE_EXPIRED. If the license string is not valid, this method returns KMC_IP_LICENSE_INVALID.
 * Any failure to set the license would automatically set the daysRemaining to zero.The license can only be set once.
 *
 * @param {Function} successCallback - Default Success call back function name
 * @param {Function} errorCallback - Default Error call back function name
 * @param {String} licenseString - is the valid license string that was contained in a distributed header file that you received from Kofax.
 *
 *
 * @return
 * The return value is captured in the 'successCallback' for a successful operation, and might return in 'errorCallback' for an incomplete/invalid operation. Returns the following values in the corresponding functions
 * successCallback -  KMC_SUCCESS    The license string was set successfully .
 * errorCallback -    KMC_IP_LICENSE_INVALID if the license string used is nil.
 * KMC_IP_LICENSE_ALREADY_SET if you have already set the license.
 * EVRS_IP_LICENSING_FAILURE if the license string is invalid for setting a license.
 * EVRS_IP_LICENSE_EXPIRATION_ERROR if the time limit of your license has expired.
 *
 * Example code follows showing how to set your license.
 *
 * @example
 * var licenseString = 'pasteyourlicensestringhere!';
 *
 * function successCallback(result){
 *       alert(JSON.stringify(result));
 * }
 * function errorCallback(error){
 *       alert(JSON.stringify(error));
 * }
 *
 * var LicObj = kfxCordova.kfxUtilities.createLicense();
 * LicObj.setMobileSDKLicense(successCallback,errorCallback,licenseString);
 */
License.prototype.setMobileSDKLicense = function(successCallback, errorCallback, licenseString) {
    ActionUtils.exec(
        function(result) {
            if (successCallback)
                successCallback(result);
        },
        function(error) {
            if (errorCallback)
                errorCallback(error);
        },
        ActionUtils.serviceName,
        ActionUtils.setSDKLicense, [licenseString]
    );
};

/// To get  the number of days the License of the SDK is valid
/**
 * Method to get the remaining  days that the SDK license would be valid to use.
 *
 * @param {Function} successCallback - Default Success call back function name
 * @param {Function} errorCallback - Default Error call back function name
 *
 *
 * @return The return value is captured in the 'successCallback' function and will be having the number of days remaining in the current license for a valid license.
 *
 * @see Check the 'errorCallback' method for any failures in case of unexpected behaviour of the method. Generally the error call back
 * would return a JSON object with ErrorMsg & ErrorDesc' giving the description of the error.
 *
 * @example
 * function successCallback(result){
 *    alert(JSON.stringify(result));
 * }
 * function errorCallback(error){
 *    alert(JSON.stringify(error));
 * }
 *
 * var LicObj = kfxCordova.kfxUtilities.createLicense();
 * var licenseString = 'pasteyourlicensestringhere!';
 * LicObj.setMobileSDKLicense(successCallback,errorCallback,licenseString);
 * LicObj.getDaysRemaining(successCallback,errorCallback);
 */
License.prototype.getDaysRemaining = function(successCallback, errorCallback) {
    ActionUtils.exec(
        function(result) {
            if (successCallback)
                successCallback(result);
        },
        function(error) {
            if (errorCallback)
                errorCallback(error);
        },
        ActionUtils.serviceName,
        ActionUtils.getDaysRemaining, []
    );
};

/// To get access to version information for the Kofax Mobile SDK
/**
 * Method to get version string for the Kofax Mobile SDK as a whole.
 * It also provides access to the version information of the Utility package and the Kofax Image Processor and Classifier libraries
 *
 * @param {Function} successCallback - Default Success call back function name
 * @param {Function} errorCallback - Default Error call back function name
 *
 * @return JSON object representing version string is returned in the 'successCallback' function.
 *
 * @see Check the 'errorCallback' method for any failures in case of unexpected behaviour of the method. Generally the error call back
 * would return a JSON object with ErrorMsg & ErrorDesc' giving the description of the error.
 *
 * @example
 * function successCallback(result){
 *       alert(JSON.stringify(result));
 * }
 * function errorCallback(error){
 *       alert(JSON.stringify(error));
 * }
 * var LicObj = kfxCordova.kfxUtilities.createLicense();
 * var licenseString = 'pasteyourlicensestringhere!';
 * LicObj.setMobileSDKLicense(successCallback,errorCallback,licenseString);
 * LicObj.getSDKVersions(successCallback,errorCallback);
 */
License.prototype.getSDKVersions = function(successCallback, errorCallback) {
    ActionUtils.exec(
        function(result) {
            if (successCallback)
                successCallback(result);
        },
        function(error) {
            if (errorCallback)
                errorCallback(error);
        },
        ActionUtils.serviceName,
        ActionUtils.getSDKVersions, []
    );
};


// ===================================================
//! Set up the SDK license server
/**
 * @private
 * The function is used to setup the url string of license server used for on device operation licensing. Application must call this function before any on device operation that requires license
 *
 * @param {Function} successCallback - Default Success call back function name
 * @param {Function} errorCallback - Default Error call back function name
 * @param {Object} licenseParameters - should be a valid url string of the on device operation licensing server and the server type.
 *
 * @param {String} [licenseParameters.serverUrl] a valid url string of the on device operation licensing server
 *
 * @param {String} [licenseParameters.serverType] type of the server
 *
 * @example
 *  var LicObj = kfxCordova.kfxUtilities.createLicense();
 *
 *  var serverType = LicObj.getServerTypes();
 *   var licenseParameters={
 *       serverUrl  : 'http://mobile-rtti/mobilesdk',
 *       serverType : serverType.RTTI
 *   }
 *
 * function successCallback(result){
 *   alert(JSON.stringify(result));
 * }
 * function errorCallback(error){
 *   alert(JSON.stringify(error));
 * }
 *
 * LicObj.setMobileSDKLicenseServer(successCallback,errorCallback,licenseParameters);
 */
License.prototype.setMobileSDKLicenseServer = function(successCallback, errorCallback, licenseParameters) {
    ActionUtils.exec(
        function(result) {
            if (successCallback)
                successCallback(result);
        },
        function(error) {
            if (errorCallback)
                errorCallback(error);
        },
        ActionUtils.serviceName,
        ActionUtils.setMobileSDKLicenseServer, [licenseParameters]
    );
};

// ===================================================
//! Get remaining license count
/**
 * @private
 * The function will return available license count for the offline operation, application should
 *
 * @param {Function} successCallback Default Success call back function name
 * @param {Function} errorCallback Default Error call back function name
 * @param {String} licenseType - for which the remaining license count is needed.
 *
 * @example
 * var LicObj = kfxCordova.kfxUtilities.createLicense();
 * var licenseType = LicObj.getLicenseTypes();
 * var licenseString = 'pasteyourlicensestringhere!';
 * function successCallback(result){
 *   alert(JSON.stringify(result));
 * }
 * function errorCallback(error){
 *   alert(JSON.stringify(error));
 * }
 * LicObj.setMobileSDKLicenseServer(successCallback,errorCallback,licenseString);
 * LicObj.getRemainingVolumeCount(successCallback,errorCallback,licenseType.ID_EXTRACTION);
 */
License.prototype.getRemainingVolumeCount = function(successCallback, errorCallback, licenseType) {
    ActionUtils.exec(
        function(result) {
            if (successCallback)
                successCallback(result);
        },
        function(error) {
            if (errorCallback)
                errorCallback(error);
        },
        ActionUtils.serviceName,
        ActionUtils.getRemainingVolumeCount, [licenseType]
    );
};


/// Method to add the event listener to the 'AcquireVolumeLicense' changed delegate method of the License
/**
 * @private
 * This method would receive the volume of license allowed for offline operation and are captured in success call back.
 *
 * @param {Function} successCallback Default Success Call back function name
 * @param {Function} errorCallback - Default Error Call back function name
 * @param {Function} acquireVolumeListenerCallBack - a var to hold the licAcquired value returned from License
 *
 * @return
 * The return value is captured in the 'successCallback' for a successful operation, and might return in 'errorCallback' for an incomplete/invalid operation. Returns the following values in the corresponding functions
 * successCallback -  KMC_SUCCESS    success call back.
 * errorCallback -    error message would contain the appropriate error description.Possible error objects are Wrong Parameters, KmcRuntimeException & Exception.
 * acquireVolumeListenerCallBack - will have the JSON object giving the volume of license allowed when the call is returned with no error.
 *
 * @see Check the 'errorCallback' method for any failures in case of unexpected behaviour of the method. By Default,  the error call back
 * would return a JSON object with ErrorMsg & ErrorDesc' giving the description of the error.
 *
 * @example
 * var LicObj = kfxCordova.kfxUtilities.createLicense();
 * function acquireVolumeListenerCallBack(licAcquired){
 *    alert(licAcquired);
 * }
 * function successCallback(result){
 *   alert(JSON.stringify(result));
 * }
 * function errorCallback(error){
 *   alert(JSON.stringify(error));
 * }
 * LicObj.addAcquireVolumeLicenseListener(successCallback,errorCallback,acquireVolumeListenerCallBack);
 */
License.prototype.addAcquireVolumeLicenseListener = function(successCallback, errorCallback, acquireVolumeListenerCallBack) {
    ActionUtils.exec(
        function(result) {
            if (result.eventType === "eventRegistered") {
                if (successCallback)
                    successCallback(result);
            } else if (result.eventType === "eventRaised") {
                acquireVolumeListenerCallBack(result);
            }
        },
        function(error) {
            if (errorCallback)
                errorCallback(error);
        },
        ActionUtils.serviceName,
        ActionUtils.addAcquireVolumeLicenseListener, []
    );
};

/// Method to remove the event listener to the 'acquireVolumeLicenses' method of the   License
/**
 * @private
 * The method would remove the listener to the delegate call back of the acquireVolumeLicenses method. After removing the listener,
 * there will not be any call backs from native from the 'acquireVolumeLicenses' delegate methods even when  it is being called in native.
 *
 * @param {Function} successCallback - Default Success Call back function name
 * @param {Function} errorCallback - Default Error Call back function name
 *
 * @return
 * The return value is captured in the 'successCallback' for a successful operation, and might return in 'errorCallback' for an incomplete/invalid operation. Returns the following values in the corresponding functions
 * successCallback -  KMC_SUCCESS success call back
 * errorCallback -    error message would contain the appropriate error description.Possible error objects are KmcRuntimeException,Exception.
 *
 * @example
 * function successCallback(result){
 *   alert(JSON.stringify(result));
 * }
 * function errorCallback(error){
 *   alert(JSON.stringify(error));
 * }
 * var LicObj = kfxCordova.kfxUtilities.createLicense();
 * LicObj.removeAcquireVolumeLicenseListener(successCallback,errorCallback);
 */
License.prototype.removeAcquireVolumeLicenseListener = function(successCallback, errorCallback) {
    ActionUtils.exec(
        function(result) {
            if (successCallback)
                successCallback(result);
        },
        function(error) {
            if (errorCallback)
                errorCallback(error);
        },
        ActionUtils.serviceName,
        ActionUtils.removeAcquireVolumeLicenseListener, []
    );
};

// ===================================================
//! Preallocate volume licenses for offline operation
/**
 * @private
 * The function is used to preallocate volume licenses for offline operation
 *
 * @param {Function} successCallback - Default Success call back function name
 * @param {Function} errorCallback - Default Error call back function name
 * @param {Object} parameters - JSON object which contains license type, Number of volume license requested
 *
 * @param {String} [parameters.licenseFeatureType] license type
 *
 * @param {Number} [parameters.licenseVolume] Number of licenses required
 *
 * @example
 * var licenseType = LicObj.getLicenseTypes();
 * function successCallback(result){
 *   alert(JSON.stringify(result));
 * }
 * function errorCallback(error){
 *   alert(JSON.stringify(error));
 * }
 * var parameters = {
 *   licenseFeatureType: licenseType.ID_EXTRACTION,
 *   licenseVolume: 20
 * };
 *
 * var LicObj = kfxCordova.kfxUtilities.createLicense();
 * LicObj.setMobileSDKLicenseServer(successCallback,errorCallback,LicenseStr);
 * LicObj.acquireVolumeLicenses(successCallback,errorCallback,parameters);
 */

License.prototype.acquireVolumeLicenses = function(successCallback, errorCallback, parameters) {
    ActionUtils.exec(
        function(result) {
            if (successCallback)
                successCallback(result);
        },
        function(error) {
            if (errorCallback)
                errorCallback(error);
        },
        ActionUtils.serviceName,
        ActionUtils.acquireVolumeLicenses, [parameters]
    );
};
//End of License Methods
module.exports = License;
