//
//  KFXPassportCaptureExperienceCriteriaHolder.h
//  kfxLibUIControls
//
//  Copyright (c) 2014-2017 Kofax. All rights reserved. Kofax Confidential.
//  Unauthorized use, duplication, or distribution, or disclosure is strictly prohibited.
//

#import <kfxLibUIControls/kfxKUIDocumentBaseCaptureExperienceCriteriaHolder.h>

@class KFXPassportDetectionSettings;

//! This class allows the passport related criteria to be configured.
/**
 This class allows all of the capture related parameters to be configured.
 */
@interface KFXPassportCaptureExperienceCriteriaHolder : kfxKUIDocumentBaseCaptureExperienceCriteriaHolder

/// Passport detection settings object.
/**
 A collection of values that control the behavior of passport detection experience. Pass nil for the experience of default setting.
 */
@property (nonatomic, retain) KFXPassportDetectionSettings* passportDetectionSettings;

@end
